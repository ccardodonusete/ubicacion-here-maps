function moveMapToBerlin(map) {
  map.setCenter({ lat: 4.6475802, lng: -74.1057188 });
  map.setZoom(15);
}

var platform = new H.service.Platform({
  apikey: 'Uw1p92zaaqqMd8w87nderAZeIQ42PT9LAY0YOCVkZq8' // replace with your api key
});
var defaultLayers = platform.createDefaultLayers();

// Initialize the map
var map = new H.Map(document.getElementById('map'), defaultLayers.vector.normal.map, {
  center: { lat: 50, lng: 5 },
  zoom: 4,
  pixelRatio: window.devicePixelRatio || 1
});
window.addEventListener('resize', () => map.getViewPort().resize());

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
var ui = H.ui.UI.createDefault(map, defaultLayers);

window.onload = function () {
  moveMapToBerlin(map);
  getDefaultLocation();
};

const autosuggest = (e) => {
  if (event.metaKey) {
      return;
  }

  let searchString = e.value;
  if (searchString != "") {
      fetch(
          `https://autosuggest.search.hereapi.com/v1/autosuggest?apiKey=${'Uw1p92zaaqqMd8w87nderAZeIQ42PT9LAY0YOCVkZq8'}&at=33.738045,73.084488&limit=5&resultType=city&q=${searchString}&lang=en-US`
      )
          .then((res) => res.json())
          .then((json) => {
              if (json.length != 0) {
                  document.getElementById("list").innerHTML = ``;
                  let dropData = json.items.map((item) => {
                      if ((item.position != undefined) & (item.position != ""))
                          document.getElementById("list").innerHTML += `<li onClick="addMarkerToMap(${item.position.lat},${item.position.lng},'${item.title}')">${item.title}</li>`;
                  });
              }
          });
  }
};

function getDefaultLocation() {
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
          var lat = position.coords.latitude;
          var lng = position.coords.longitude;
          var title = "";
          addMarkerToMap(lat, lng, title, true);
          console.log("Latitud: " + lat + ", Longitud: " + lng);
      });
  } else {
      var lat = 4.6475802;
      var lng = -74.1057188;
      var title = "Bogota D.C";
      addMarkerToMap(lat, lng, title, true);
  }
}

let originMarker = null;
let destinationMarker = null;
let routeLine = null;
let routeMarkers = [];

const addMarkerToMap = (lat, lng, title, isOrigin = false) => {
  document.getElementById("search").value = title;

  if (isOrigin) {
      if (originMarker) {
          map.removeObject(originMarker);
      }
      originMarker = new H.map.Marker({ lat, lng }, { data: 'origin' });
      map.addObject(originMarker);
      map.setCenter({ lat, lng }, true);
  } else {
      if (destinationMarker) {
          map.removeObject(destinationMarker);
      }
      destinationMarker = new H.map.Marker({ lat, lng }, { data: 'destination' });
      map.addObject(destinationMarker);
      map.setCenter({ lat, lng }, true);

      if (originMarker) {
          calculateRouteFromAtoB(platform, originMarker.getGeometry().lat, originMarker.getGeometry().lng, lat, lng);
      }
  }
  document.getElementById("list").innerHTML = ``;
};

function calculateRouteFromAtoB(platform, originLat, originLng, destLat, destLng) {
  var router = platform.getRoutingService(null, 8),
      routeRequestParams = {
          routingMode: 'fast',
          transportMode: 'car',
          origin: originLat + ',' + originLng,
          destination: destLat + ',' + destLng,
          return: 'polyline,turnByTurnActions,actions,instructions,travelSummary'
      };

  router.calculateRoute(routeRequestParams, onSuccess, onError);
}

function onSuccess(result) {
  var route = result.routes[0];
  addRouteShapeToMap(route);
  addManueversToMap(route);
  addWaypointsToPanel(route);
  addManueversToPanel(route);
  addSummaryToPanel(route);
}

function onError(error) {
  alert('Can\'t reach the remote server');
}

function addRouteShapeToMap(route) {
  if (routeLine) {
      map.removeObject(routeLine);
  }

  route.sections.forEach((section) => {
      let linestring = H.geo.LineString.fromFlexiblePolyline(section.polyline);
      routeLine = new H.map.Polyline(linestring, {
          style: {
              lineWidth: 4,
              strokeColor: 'rgba(0, 128, 255, 0.7)'
          }
      });
      map.addObject(routeLine);
      map.getViewModel().setLookAtData({
          bounds: routeLine.getBoundingBox()
      });
  });

  routeMarkers.forEach(marker => map.removeObject(marker));
  routeMarkers = [];
}

function addManueversToMap(route) {
  if (routeMarkers.length > 0) {
      routeMarkers.forEach(marker => map.removeObject(marker));
      routeMarkers = [];
  }

  var svgMarkup = '<svg width="18" height="18" ' +
      'xmlns="http://www.w3.org/2000/svg">' +
      '<circle cx="8" cy="8" r="8" ' +
      'fill="#1b468d" stroke="white" stroke-width="1" />' +
      '</svg>',
      dotIcon = new H.map.Icon(svgMarkup, { anchor: { x: 8, y: 8 } }),
      group = new H.map.Group();

  route.sections.forEach((section) => {
      let poly = H.geo.LineString.fromFlexiblePolyline(section.polyline).getLatLngAltArray();
      let actions = section.actions;
      actions.forEach((action, idx) => {
          let marker = new H.map.Marker({
              lat: poly[action.offset * 3],
              lng: poly[action.offset * 3 + 1]
          }, { icon: dotIcon });
          marker.instruction = action.instruction;
          group.addObject(marker);
          routeMarkers.push(marker);
      });

      group.addEventListener('tap', function (evt) {
          map.setCenter(evt.target.getGeometry());
          openBubble(evt.target.getGeometry(), evt.target.instruction);
      }, false);

      map.addObject(group);
  });
}

function addWaypointsToPanel(route) {
  var nodeH3 = document.createElement('h3'),
      labels = [];

  route.sections.forEach((section) => {
      labels.push(
          section.turnByTurnActions[0].nextRoad.name[0].value)
      labels.push(
          section.turnByTurnActions[section.turnByTurnActions.length - 1].currentRoad.name[0].value)
  });

  nodeH3.textContent = labels.join(' - ');
  routeInstructionsContainer.innerHTML = '';
  routeInstructionsContainer.appendChild(nodeH3);
}

function addSummaryToPanel(route) {
  let duration = 0,
      distance = 0;

  route.sections.forEach((section) => {
      distance += section.travelSummary.length;
      duration += section.travelSummary.duration;
  });

  var summaryDiv = document.createElement('div'),
      content = '<b>Total distance</b>: ' + distance + 'm. <br />' +
          '<b>Travel Time</b>: ' + toMMSS(duration) + ' (in current traffic)';

  summaryDiv.style.fontSize = 'small';
  summaryDiv.style.marginLeft = '5%';
  summaryDiv.style.marginRight = '5%';
  summaryDiv.innerHTML = content;
  routeInstructionsContainer.appendChild(summaryDiv);
}

function addManueversToPanel(route) {
  var nodeOL = document.createElement('ol');

  nodeOL.style.fontSize = 'small';
  nodeOL.style.marginLeft = '5%';
  nodeOL.style.marginRight = '5%';
  nodeOL.className = 'directions';

  route.sections.forEach((section) => {
      section.actions.forEach((action, idx) => {
          var li = document.createElement('li'),
              spanArrow = document.createElement('span'),
              spanInstruction = document.createElement('span');

          spanArrow.className = 'arrow ' + (action.direction || '') + action.action;
          spanInstruction.innerHTML = section.actions[idx].instruction;
          li.appendChild(spanArrow);
          li.appendChild(spanInstruction);

          nodeOL.appendChild(li);
      });
  });

  routeInstructionsContainer.appendChild(nodeOL);
}

function toMMSS(duration) {
  return Math.floor(duration / 60) + ' minutes ' + (duration % 60) + ' seconds.';
}

function openBubble(position, text) {
  if (!bubble) {
      bubble = new H.ui.InfoBubble(position, {
          content: text
      });
      ui.addBubble(bubble);
  } else {
      bubble.setPosition(position);
      bubble.setContent(text);
      bubble.open();
  }
}

// DOM elements creation
var mapContainer = document.createElement('div');
mapContainer.id = 'mapContainer';
document.body.appendChild(mapContainer);

var mapElement = document.createElement('div');
mapElement.id = 'map';
mapElement.style = 'width: 80%; height: 100vh; float: left;';
mapContainer.appendChild(mapElement);

var panelElement = document.createElement('div');
panelElement.id = 'panel';
panelElement.style = 'width: 20%; height: 100vh; float: left; overflow-y: auto;';
mapContainer.appendChild(panelElement);

var searchElement = document.createElement('input');
searchElement.type = 'text';
searchElement.id = 'search';
searchElement.onkeyup = (event) => autosuggest(event.target);
mapElement.appendChild(searchElement);

var listElement = document.createElement('ul');
listElement.id = 'list';
mapElement.appendChild(listElement);

var routeInstructionsContainer = document.createElement('div');
routeInstructionsContainer.id = 'panel';
routeInstructionsContainer.style = 'width: 100%; height: 100vh; overflow-y: auto;';
panelElement.appendChild(routeInstructionsContainer);

var bubble;
